from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
CORS(app)
# CORS(app) pour acces toute les routes. IMPORT CROS
# @cross_origin() pour acces une route en particulier IMPORT cross_origin


################## La liste des users ##################
userList = {'users': 
[
    { 'id': 1, 'lastName': 'Snow', 'firstName': 'Jon', 'valid': True},
    { 'id': 2, 'lastName': 'Lannister', 'firstName': 'Cersei', 'valid': True},
    { 'id': 3, 'lastName': 'Adel', 'firstName': 'KACIMI', 'valid': False},
    { 'id': 4, 'lastName': 'Stark', 'firstName': 'Arya', 'valid': False},
    { 'id': 5, 'lastName': 'Targaryen', 'firstName': 'Daenerys', 'valid': False},
    { 'id': 6, 'lastName': 'Melisandre', 'firstName': 'andréa', 'valid': True},
    { 'id': 7, 'lastName': 'Clifford', 'firstName': 'Ferrara', 'valid': False},
    { 'id': 8, 'lastName': 'Frances', 'firstName': 'Rossini', 'valid': True},
    { 'id': 9, 'lastName': 'Roxie', 'firstName': 'Harvey', 'valid': False},
]}

################### page d'accueil ##################
@app.route('/', methods=['GET'])
def hello_world():
    return '''<h1>Distant Reading Users</h1>
            <p>A prototype API for distant reading users list.</p>
            <a href="http://127.0.0.1:5000/api/v1/users/all">get users</a>'''

################### Suppression user ##################
@app.route('/api/v1/users/delete/<int:idUser>', methods=['DELETE'])
def delete_user(idUser):
    del userList["users"][idUser]
    return(userList)

################### Edit user ##################
@app.route('/api/v1/users/edit/<int:idUser>', methods=['PUT','PATCH'])
def edit_user(idUser):
    if (request.method=='PUT'):
        data = json.loads(request.data)
        userList['users'][idUser]['firstName'] = data['textFirst']
        userList['users'][idUser]['lastName'] = data['textLast']
        return(userList)
    else:
        data = json.loads(request.data)
        userList['users'][idUser]['valid'] = data['valid']
        return(userList)

################## GET/POST users ##################
@app.route('/api/v1/users/all', methods=['GET','POST'])
def get_users():
    if (request.method=='POST'):
        data = json.loads(request.data)
        userList["users"].append(data)
        return(userList)
    else :
        return jsonify(userList)


################## Réucpération d'un users par Id ####################
@app.route('/api/v1/users', methods=['GET'])
def api_id():
    if 'id' in request.args:
        id = int(request.args['id'])
    else:
        return "Error: No id field provided. Please specify an id."

    for user in userList['users']:
        if user['id'] == id:
            return jsonify(user)
    return jsonify([])


################## Run APP ##################
if __name__ == '__main__':
    app.run(debug=True, port=5000)